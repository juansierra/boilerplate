<?php
namespace Checks;

use BeyondCode\SelfDiagnosis\Checks\Check;

class DiskSpace implements Check
{
    private $errorMessage = '';

    /**
     * The name of the check.
     *
     * @param array $config
     * @return string
     */
    public function name(array $config): string
    {
        return 'There is enough space on disk.';
    }

    /**
     * Perform the actual verification of this check.
     *
     * @param array $config
     * @return bool
     */
    public function check(array $config): bool
    {
        $path = $config['path'];
        $minimum = $config['minimum'];
        $free = disk_free_space($path);

        if (! $this->isEnough($free, $minimum)) {
            return $this->makeResult(
                false,
                sprintf(
                    'Volume %s is getting out of space. It has %s when it should have at least %s.',
                    $path,
                    $this->bytes2human($free),
                    $minimum
                )
            );
        }

        return true;
    }

    /**
     * @param  string  $free
     * @param  string  $minimum
     * @return boolean
     */
    private function isEnough(string $free, string $minimum)
    {
        return $free > $this->human2bytes($minimum);
    }

    /**
     * Convert bytes to human readable.
     *
     * @return string
     */
    private function bytes2human($bytes)
    {
        $base = log($bytes) / log(1024);
        $suffix = ['', 'KB', 'MB', 'GB', 'TB'];
        $f_base = floor($base);

        return round(pow(1024, $base - floor($base)), 1) . $suffix[$f_base];
    }

    /**
    * Convert bytes to human readable.
    *
    * @return string
    */
    private function human2bytes($str)
    {
        $str = trim($str);

        $num = (float) $str;

        if (strtoupper(substr($str, -1)) == 'B') {
            $str = substr($str, 0, -1);
        }

        switch (strtoupper(substr($str, -1))) {
            case 'P':
                $num *= 1024; //no break intended
            case 'T':
                $num *= 1024; //no break intended
            case 'G':
                $num *= 1024; //no break intended
            case 'M':
                $num *= 1024; //no break intended
            case 'K':
                $num *= 1024;
        }

        return $num;
    }

    /**
     * The error message to display in case the check does not pass.
     *
     * @param array $config
     * @return string
     */
    public function message(array $config): string
    {
        return $this->errorMessage;
    }
}
