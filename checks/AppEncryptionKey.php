<?php
namespace Checks;

use BeyondCode\SelfDiagnosis\Checks\Check;

class AppEncryptionKey implements Check
{
    /**
     * The name of the check.
     *
     * @param array $config
     * @return string
     */
    public function name(array $config): string
    {
        return 'Application Encryption Key exists';
    }

    /**
     * Perform the actual verification of this check.
     *
     * @param array $config
     * @return bool
     */
    public function check(array $config): bool
    {
        return ('' != env('APP_KEY', ''));
    }

    /**
     * The error message to display in case the check does not pass.
     *
     * @param array $config
     * @return string
     */
    public function message(array $config): string
    {
        return 'Application Encryption Key is missing. Please run php artisan key:generate';
    }
}
