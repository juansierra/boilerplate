<?php
namespace Checks;

use SensioLabs\Security\SecurityChecker as SensioLabsSecurityChecker;
use BeyondCode\SelfDiagnosis\Checks\Check;

class UnsafePackages implements Check
{
    private $errorMessage = '';

    /**
     * The name of the check.
     *
     * @param array $config
     * @return string
     */
    public function name(array $config): string
    {
        return 'There are not unsafe packages';
    }

    /**
     * Perform the actual verification of this check.
     *
     * @param array $config
     * @return bool
     */
    public function check(array $config): bool
    {
        $checker = new SensioLabsSecurityChecker();

        $alerts = $checker->check(base_path('composer.lock'));

        if (0 == count($alerts)) {
            return true;
        }

        $problems = collect($alerts)
          ->keys()
          ->implode(', ');

        $this->errorMessage = sprintf($this->target->getErrorMessage(), $problems);

        return false;
    }

    /**
     * The error message to display in case the check does not pass.
     *
     * @param array $config
     * @return string
     */
    public function message(array $config): string
    {
        return $this->errorMessage;
    }
}
