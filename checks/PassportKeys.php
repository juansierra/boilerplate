<?php
namespace Checks;

use BeyondCode\SelfDiagnosis\Checks\Check;

use Illuminate\Support\Facades\Storage;

class PassportKeys implements Check
{
    /**
     * The name of the check.
     *
     * @param array $config
     * @return string
     */
    public function name(array $config): string
    {
        return 'Passport Keys exist';
    }

    /**
     * Perform the actual verification of this check.
     *
     * @param array $config
     * @return bool
     */
    public function check(array $config): bool
    {
        $private = storage_path('oauth-private.key');
        $public = storage_path('oauth-public.key');

        return (
          file_exists($private) &&
          file_exists($public)
        );
    }

    /**
     * The error message to display in case the check does not pass.
     *
     * @param array $config
     * @return string
     */
    public function message(array $config): string
    {
        return 'Passport keys are missing. Please run php artisan passport:keys';
    }
}
