<?php

Route::namespace('\App\Http\V1\Controllers')
  ->group(function() {

    Route::prefix('auth')->group(function () {
      Route::post('login', 'AuthController@login');

      Route::middleware('auth:api')->group (function () {
          Route::get('me', 'AuthController@me');
      });
    });

    Route::middleware('guest')
      ->group(function () {
        Route::get('/status', 'StatusController@index');
    });

    Route::middleware('auth:api')->group (function () {

      // Route::put('/tasks/{task}', function (\App\Task $task) {
      //     dd('pl');
      // })->middleware('can:update');

      // Route::apiResource('tasks', 'TaskController')->only([
      //   'index', 'update', 'store', 'destroy'
      // ]);

      Route::resource('tasks', 'TaskController')->only([
          'index', 'update', 'store', 'destroy'
      ]);

      // Route::put('/tasks/{taskId}', 'TaskController@update')
      //   ->middleware('can:update,taskId');

    });

});
