<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// Route::middleware('auth:api')->get('/user', function (Request $request) {
//     return $request->user();
// });

// Route::prefix('auth')->group(function () {
//   Route::post('login', 'AuthController@login');
//   // Route::middleware('auth:api')->group (function () {
//   //     Route::get('me', 'AuthController@me');
//   // });
// });

Route::middleware('api.version:1')
  ->prefix('v1')
  ->group(function() {
    require base_path('routes/api_v1.php');
});

// Route::middleware('api.version:2')
//   ->prefix('v2')
//   ->group(function() {
//     require base_path('routes/api_v2.php');
// });
