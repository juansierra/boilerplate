# API Boilerplate

This is a lightweight [Laravel 5.8](https://laravel.com/) boilerplate API.

## Getting Started

This project has been put together with the intention of being cloned for each new API project you start working on.

It contains a basic API for a To-Do task list, so you can have a basic example of each API component to copy-paste from.

1. Clone the repository,
2. Run `composer update`
3. Copy `env.example` to `.env` and configure your env variables.
4. Run `php artisan migrate --seed`
5. Run `php artisan passport:install`
6. Run `php artisan passport:client --personal`
7. Run `php artisan self-diagnosis`

> TODO: Add [laradock](https://laradock.io/documentation/) support

## Index

* [Directory structure](#directory-structure)
  * [Root](#root)
  * [App](#app)
* [Self-Diagnosis](#self-diagnosis)
   * [Running the checks](#running-the-checks)
   * [Writting custom checks](#writting-custom-checks)
* [Authentication](#authentication)
   * [Token lifetimes](#token-lifetimes)
* [Authorization](#authorization)
   * [Roles](#roles)
* [Versioning](#versioning)
   * [Routes](#routes)
      * [Unprotected](#unprotected)
      * [Protected](#protected)
      * [Resources](#resources)
* [Http](#http)
   * [/Vx/Controllers](#vxcontrollers)
   * [/Vx/Requests](#vxrequests)
   * [/Vx/Resources](#vxresources)
* [Exceptions](#exceptions)
* [Services](#services)
* [Tests](#tests)
   * [Running the tests](#running-the-tests)
* [Documentation](#documentation)
* [OpenApi](#openapi)
* [Postman](#postman)
* [Artisan Commands](#artisan-commands)
   * [user:create](#usercreate)

## Directory structure

The standard [Laravel directory structure](https://laravel.com/docs/5.8/structure) remains as original with some additions.

### Root

* `app` contains the core code of your application.
* `config` contains all of your application's configuration files.
* `database` contains your database migrations, model factories, and seeds.
* `routes` contains all of the route definitions for your application.
* `tests` contains your automated tests.
* `checks` contains your automated self-diagnosis checks.
* `doc` contains all of your application's documentation.

### App

* `Console` contains all of the custom Artisan commands for your application.
* `Events` contains your Event definitions.
* `Exceptions` contains your application's exception handler and also any custom exceptions you might want to use.
* `Http` contains your controllers, middleware, form requests, and resource serializers.
* `Listeners` contains your Event listeners.
* `Models` does not contain your models, but any model traits you want to use.
* `Observers` contains all your model observers.
* `Policies` contains all your authorization policies.
* `Providers` contains your custom service providers.
* `Services` contains most of your business logic as services.

> Note: It is recommended that any core functionality of your application resides in the App folder.

## Self-Diagnosis

Everytime you setup your API from scratch you can use the [self-diagnosis checks](https://github.com/beyondcode/laravel-self-diagnosis) to make sure everything is setup correctly.

Please note that self-diagnosis checks are not unit or feature tests. The purpose of these checks is to assert that the API is configured properly.

### Running the checks

Run `artisan self-diagnosis` command to check everything is setup correctly.

The command runs several standard checks like access to the database, .env file, etc., but also any custom checks you may need for your project. i.e. access to 3rd party APIs, etc.

### Writting custom checks

Some custom checks already exist in `/checks` folder.

* `AppEncriptionKey` checks the `APP_KEY` env variable has been set.
* `DiskSpace` checks there is enough space disk available.
* `PassportKeys` checks the passport oauth keys has been created.
* `UnsafePackages` check that there are not packages in your composer file that have been declared as unsafe by [SensioLabs Security Checker](https://github.com/sensiolabs/security-checker)

These custom checks serve a good example should you need to write your own ones. A common custom check is make sure your credentials for 3rd party APIs are correct.

Once you have written your own custom check, remember adding it to the list at `/config/self-diagnosis.php`.

## Authentication

> TODO: Provide more examples with Token Scopes and different Grant types.

Laravel provides a full OAuth2 server implementation called [Passport](https://laravel.com/docs/5.8/passport)

### Token lifetimes

> TODO: Change demo project to use minutes instead of days

By default, Passport issues long-lived access tokens that expire after one year. If you would like to configure a longer / shorter token lifetime, you may change the default values at `/config`.

```
`tokens` => [
  'expiresInDays' => env('TOKENS_EXPIRES_IN_DAYS', 15),
  'refreshExpiresInDays' => env('TOKENS_REFRESH_EXPIRES_IN_DAYS', 30),
],
```

## Authorization

Once your user has a valid token, it's up to you to decide what actions he or she is able to perform on the different resources.

This task is very easy using [Laravel Policies](https://laravel.com/docs/5.8/authorization#creating-policies).

In the project demo you can find a basic Policy set for the task resource at `/app/Policies/TaskPolicy.php`.

```
public function update(User $user, Task $task)
{
    return intval($user->id) === intval($task->user_id);
}
```

Basically this policy specifies that:

* Users can view, edit, delete and restore their own tasks.
* All users can create and list tasks.
* All users with God privilege can do everything.

There are different ways in your code where you can check the user permissions on a specific route, a given resource, or any other action you might define. In the project demo this check is done in the business logic, before performing any operation with Tasks.

```
public function updateTask(int $taskId, Array $taskDetails)
{
    $task = Task::findOrFail($taskId);

    $this->authorize('update', $task);
    ...
}
```

If the authed user is not allowed to peform an `update` on the given `$task`, it will throw an Authorization exception.

### Roles

It is common to many APIs to assign users different roles to group certain privileges.

In the project demo there is an example of this, which is achieved by:

* Database migrations to create `roles` and `roles_users` (n-to-n) tables
* Database seed to populate the initial roles availables.
* `User` and `Role` models.
* `HasRole` trait to provide with helpers like `$user->hasRole(Role::GOD)`

An example of how to check for a user role to grant access to resources can be found in the example Task policy:

```
public function before(User $user, string $ability)
{
    if ($user->hasRole(Role::GOD)) {
        return true;
    }
}
```

## Versioning

> TODO: Provide an example of V2 inheriting V1 routes.

It is a good practice to separate your API route into different version prefixes in order to not break backwards compatibility when developing new features.

The project demo includes an example for a `v1` prefix which is achieved by:

* All code for `v1` resides at `app/Http/V1`
* `ApiVersion` middleware makes the requested route version available in your controller or business logic.
* A group definition for your `v1` routes in `routes/api.php`:
* A single route definition for the previous group in `routes/api_v1.php`:
```
Route::middleware('api.version:1')
  ->prefix('v1')
  ->group(function() {
      require base_path('routes/api_v1.php');
    });
```
```
Route::namespace('\App\Http\V1\Controllers')
  ->group(function() {
      ...
    });
```

### Routes

It is important to keep an organized, clean, updated, and minified routes definitions

There is a route definition file for each version in `/routes` folder.

Remember you can always run the `routes:list` artisan command to see the current routes, params, and middleware defined in your API.

Please note that `web` routes has been disabled in the demo project as it is an API only purpose project.

#### Unprotected

Use middleware `guest` to group all routes available to not authed users
```
Route::middleware('guest')->group(function () {
  Route::get('/status', 'StatusController@index');
});
```

#### Protected

Use middleware `auth:api` to group all routes available only to authed users
```
Route::middleware('auth:api')->group (function () {
  Route::get('me', 'AuthController@me');
});
```

#### Resources

> TODO: Provide with a paginated resource example

Use `resource` to group all CRUD functions available on resources
```
Route::resource('tasks', 'TaskController')->only([
  'index', 'update', 'store', 'destroy'
]);
```
## Http

### /Vx/Controllers

All controllers belongs to an API version, therefore they are located at `app/Http/Vx/Controllers`.

In the routes definitions you don't need to specify the controller's version for each endpoint, it is set for the whole group at the beginning of the file:
```
Route::namespace('\App\Http\V1\Controllers')
  ->group(function() {
      ...
    });
```

It is a good practice in large projects to keep your controllers as small as possible. Business logic should be encapsulated in a library such a service provider.

The controller is responsible only of:
1. Validate the request
2. Delegate business logic
3. Respond with an answer

It is important to note that a `ResponseMacroServiceProvider` has been added and used in the demo project in order to unify all the responses with the standard envelopes `data` and `error`.

### /Vx/Requests

Also requests belongs longs to an API version, therefore they are located at `app/Http/Vx/Requests`.

Please have a look to [laravel Form Requests](https://laravel.com/docs/5.8/validation#form-request-validation) for further details.

You can create a request validation for each controller's action which needs to validate its request.

It's up to you to reuse request validation among controllers or functions.

All they do is validate the request data and throw an exception if it fails.

Here is the request for creating a Task in the demo project.
```
public function rules()
{
  return [
      'name' => 'required|string',
  ];
}
```

### /Vx/Resources

Also resources belongs longs to an API version, therefore they are located at `app/Http/Vx/Resources`.

Please have a look to [Laravel Eloquent Resources](https://laravel.com/docs/5.8/eloquent-resources) for further details.

There are resources for single items and for collections.

All it does is serialize a model into an array, with any custom logic you might want to add.

Here is the resource serializer for a Task in the demo project.
```
public function toArray($request)
{
    return [
        'id' => $this->id,
        'name' => $this->name,
        'createdAt' => $this->created_at,
        'completedAt' => $this->completed_at,
        'isCompleted' => !is_null($this->completed_at),
    ];
}
```

## Exceptions

> TODO: Provide an example of custom exception thrown and catched.

The default Exception handler at `app/Exceptions/Handler` has been modified to transform every exception into an error response with `500` status code.

Unless you catch specific cases and return custom status codes and messages.

If you want to handle custom Exceptions:
1. Add your Exception declaration either in `/app/Exceptions` folder or anywhere in your business logic (i.e. a Service)
2. Catch the Exception in the default handler if you want to return a different status code than `500` for that specific exception.

This solution gives you great flexibility while keeping things very simple.

It is important to note that a custom `AcceptsJson` middleware has been created and used in order to force the exception handler to always respond with a JSON response.

## Events

It is useful sometimes to delegate some business logic into queueable actions. i.e. Log certain actions, Send a notification or email, etc.
Not only for faster API responses - as some of this tasks can be slow or depend on 3rd party providers - but also to keep your code organized.

This is achieved by using events and listeners.

First define your events in `/app/Events`. In the demo you can find `TaskCreated` and `TaskCompleted` event definitions.

```
...
```

A good place to fire your events is in an observer class, so you can quickly see where your events are fired, since usually events are created because a model has changed.

In the demo, everytime a Task is completed the `TaskCompleted` event is fired:

```
public function completed(Task $task)
{
    event(new TaskCompleted($task));
}
```

Please note that observers must be added to `/app/AppServiceProvider.php`:

```
public function boot()
{
    Task::observe(TaskObserver::class);
}
```

Next, you can listen on these events and react to them. There are different ways but a good one can be implementing a listener, so again you can group different event reactions into a single place.

In the demo there is a listener which logs certain task events at `/app/Listeners/TaskLogger.php`:

```
public function onTaskCompleted(TaskCompleted $event) {
    $task = $event->task;
    $user = $task->user;
    \Log::info("Task {$task->id} completed by user {$user->id}");
}
```

Please note that listeners must be added to `/app/Providers/EventServiceProvider.php`:
```
protected $subscribe = [
    'App\Listeners\TasksLogger',
];
```

## Services

If you want to separate business logic from your controllers, it is easy using [Laravel Services](https://laravel.com/docs/5.8/providers).

1. Define a service interface, which will help you to interchange between different versions of your service later on. The interface can be defined in `/app/Services`.

2. write your service which implements the previous interface. The service can be located in `/app/Services/YourService`.

3. write a service provider in `/app/Providers`, which will tell Laravel what Service will bind the interface with the service.

Now you can inject your Service into your controller and is ready to use. Should you need to switch to a different Service just adjust the bindings.

## Tests

> TODO: Add more OAuth tests

Tests are defined in `/tests/Feature` and you can find existing tests for the demo routes.

Please have to look to [Testing JSON APIs with Laravel](https://laravel.com/docs/5.8/http-tests#testing-json-apis) for futher details.

### Running the tests

The `test.sh` script in the root folder creates a sqlite database on disk and run the `phpunit` tests.

## Documentation

> TODO: Provide an example of converting the openapi file into static html file.
## OpenApi

The API documentation can reside in a single openapi file in the `/doc` folder.

You can use [Swagger Editor](https://editor.swagger.io/) to edit it.

The demo project provides an [OpenAPI 3](https://github.com/OAI/OpenAPI-Specification/blob/master/versions/3.0.0.md) sample file.

## Postman

It is very useful for other developers if you provide as well with a [Postman](https://www.getpostman.com/) collection and environment files in the same folder.

## Artisan Commands

The demo project comes with a handful of useful artisan commands you can not only use but steal for writing your own ones.

### user:create

Use this command to create users in your API.
