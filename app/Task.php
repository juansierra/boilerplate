<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Task extends Model
{
    /**
    * The attributes that are mass assignable.
    *
    * @var array
    */
    protected $fillable = [
        'name',
        'completed_at',
    ];

    /**
    * The attributes that should be cast to native types.
    *
    * @var array
    */
    protected $casts = [
        'completed_at' => 'datetime',
    ];

    /**
    * Array of our custom model events declared under model property $observables
    *
    * @var array
    */
    protected $observables = [
        'completed',
    ];

    /**
    * Get the post that the comment belongs to.
    *
    * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
    */
    public function user()
    {
        return $this->belongsTo('App\User');
    }

    /**
     * Completes a task
     *
     * @return Task
     */
    public function complete()
    {
        if (is_null($this->completed_at)) {
            $this->completed_at = \Carbon\Carbon::now();
            $this->save();
        }
        return $this;
    }
}
