<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class TaskListServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(
            'App\Services\TaskListInterface',
            'App\Services\TaskList\SimpleTaskList'
        );
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
