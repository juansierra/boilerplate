<?php
namespace App\Services\TaskList;

use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Support\Facades\Auth;
use App\Services\TaskListInterface;
use App\User;
use App\Task;

class SimpleTaskList implements TaskListInterface
{
    use AuthorizesRequests;

    /**
     * Runs a transaction with a number of attempts
     * @param  callable $code
     */
    protected function transaction(callable $code) {
      return \DB::transaction(
        $code,
        config('database.transactions.attempts')
      );
    }

    /**
     * Get tasks for given user
     *
     * @param  User   $user
     * @return Collection
     */
    public function getTasksForUser(User $user)
    {
        $this->authorize('index', Task::class);

        return $user->tasks()->paginate();
    }

    /**
     * Creates a task for a given user
     *
     * @param  User   $user
     * @param  Array  $taskDetails
     * @return Task
     */
    public function createTaskForUser(User $user, Array $taskDetails)
    {
        $this->authorize('create', Task::class);

        $task = $this->transaction(function () use ($user, $taskDetails) {
          $task = $user->tasks()->create($taskDetails);
          return $task;
        });

        return $task;
    }

    /**
     * Updates a task
     *
     * @param  int    $taskId
     * @param  Array  $taskDetails
     * @return Task
     */
    public function updateTask(int $taskId, Array $taskDetails)
    {
        $task = Task::findOrFail($taskId);

        $this->authorize('update', $task);

        $task = $this->transaction(function () use ($task, $taskDetails) {
          $task->update($taskDetails);

          if (isset($taskDetails['completed']) && $taskDetails['completed']) {
            $task->complete();
          }

          return $task;
        });

        return $task;
    }

    /**
     * Deletes a task
     *
     * @param  int    $taskId
     * @return Task
     */
    public function deleteTask(int $taskId)
    {
      $task = Task::findOrFail($taskId);

      $this->authorize('delete', $task);

      $task = $this->transaction(function () use ($task) {
        $task->delete();
        return $task;
      });

      return $task;
    }
}
