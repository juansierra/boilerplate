<?php
namespace App\Services;

use App\User;

interface TaskListInterface
{
  /**
   * Get tasks for given user
   *
   * @param  User   $user
   * @return Collection
   */
  public function getTasksForUser(User $user);

  /**
   * Creates a task for a given user
   *
   * @param  User   $user
   * @param  Array  $taskDetails
   * @return Task
   */
  public function createTaskForUser(User $user, Array $taskDetails);

  /**
   * Updates a task
   *
   * @param  int    $taskId
   * @param  Array  $taskDetails
   * @return Task
   */
  public function updateTask(int $taskId, Array $taskDetails);

  /**
   * Deletes a task
   *
   * @param  int    $taskId
   * @return Task
   */
  public function deleteTask(int $taskId);
}
