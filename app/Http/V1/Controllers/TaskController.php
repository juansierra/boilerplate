<?php

namespace App\Http\V1\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Http\V1\Resources\Task as TaskResource;
use App\Http\V1\Resources\TaskList as TaskListResource;
use App\Http\V1\Requests\TaskStore as TaskStoreRequest;
use App\Http\V1\Requests\TaskUpdate as TaskUpdateRequest;
use App\Services\TaskListInterface;
use App\Task;

class TaskController extends Controller
{
    /**
     * Get list of tasks
     * @param  TaskListInterface $taskList
     * @return Illuminate\Http\JsonResponse
     */
    public function index(TaskListInterface $taskList)
    {
        $tasks = $taskList->getTasksForUser(
          Auth::user()
        );

        return response()->withData(
            new TaskListResource($tasks)
        );
    }

    /**
     * Creates a task
     * @param  TaskStoreRequest $request
     * @param  TaskListInterface $taskList
     * @return Illuminate\Http\JsonResponse
     */
    public function store(TaskStoreRequest $request, TaskListInterface $taskList)
    {
        $task = $taskList->createTaskForUser(
          Auth::user(),
          $request->all()
        );

        return response()->withData(
            new TaskResource($task),
            201
        );
    }

    /**
     * Updates a task
     * @param  TaskUpdateRequest $request
     * @param  TaskListInterface $taskList
     * @param  int               $taskId
     * @return Illuminate\Http\JsonResponse
     */
    public function update(TaskUpdateRequest $request, TaskListInterface $taskList, int $taskId)
    {
        $task = $taskList->updateTask($taskId, $request->all());

        return response()->withData(
            new TaskResource($task)
        );
    }

    /**
     * Removes a task
     * @param  TaskListInterface $taskList
     * @param  int    $taskId
     * @return Illuminate\Http\JsonResponse
     */
    public function destroy(TaskListInterface $taskList, int $taskId)
    {
        $task = $taskList->deleteTask($taskId);

        return response()->withData(
            new TaskResource($task)
        );
    }
}
