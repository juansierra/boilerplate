<?php

namespace App\Http\V1\Controllers;

use Illuminate\Http\Request;
use App\Http\V1\Requests\AuthLogin as AuthLoginRequest;
use Illuminate\Support\Facades\Auth;
use App\Http\V1\Resources\Token as TokenResource;
use App\Http\V1\Resources\User as UserResource;
use Illuminate\Auth\AuthenticationException;

class AuthController extends Controller
{
    /**
     * Login with email and password
     * @param  AuthLoginRequest $request
     * @return Illuminate\Http\JsonResponse
     */
    public function login(AuthLoginRequest $request)
    {
        if (Auth::attempt([
            'email' => request('email'),
            'password' => request('password')
            ])
        ) {
            $user = Auth::user();
            $token =  $user->createToken('MyApp')-> accessToken;

            return response()->withData(
                new TokenResource([
                    'token' => $token,
                ])
            );
        } else {
              throw new AuthenticationException('Invalid credentials');
        }
    }

    /**
     * Get current authed user
     * @return Illuminate\Http\JsonResponse
     */
    public function me()
    {
        $user = Auth::user();

        return response()->withData(
            new UserResource($user)
        );
    }
}
