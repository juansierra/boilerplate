<?php

namespace App\Http\V1\Controllers;

use Illuminate\Http\Request;
use App\Http\V1\Resources\Status as StatusResource;

class StatusController extends Controller
{
    /**
     * Get API status
     * @return Illuminate\Http\JsonResponse
     */
    public function index()
    {
        return response()->withData(
            new StatusResource([
                'environment' => \App::environment(),
            ])
        );
    }
}
