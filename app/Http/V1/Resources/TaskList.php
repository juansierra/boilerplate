<?php

namespace App\Http\V1\Resources;

use Illuminate\Http\Resources\Json\ResourceCollection;

class TaskList extends ResourceCollection
{
    /**
     * The resource that this resource collects.
     *
     * @var string
    */
    public $collects = 'App\Http\V1\Resources\Task';

   /**
    * Transform the resource collection into an array.
    *
    * @param  \Illuminate\Http\Request  $request
    * @return array
   */
    public function toArray($request)
    {
        return $this->collection;
    }
}
