<?php

namespace App\Exceptions;

use Exception;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Auth\AuthenticationException;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Validation\ValidationException;
use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class Handler extends ExceptionHandler
{
    /**
     * A list of the exception types that are not reported.
     *
     * @var array
     */
    protected $dontReport = [
        //
    ];

    /**
     * A list of the inputs that are never flashed for validation exceptions.
     *
     * @var array
     */
    protected $dontFlash = [
        'password',
        'password_confirmation',
    ];

    /**
     * Report or log an exception.
     *
     * @param  \Exception  $exception
     * @return void
     */
    public function report(Exception $exception)
    {
        parent::report($exception);
    }

    /**
     * Render an exception into an HTTP response.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Exception  $exception
     * @return \Illuminate\Http\Response
     */
    public function render($request, Exception $e)
    {
        // dd($e);

        $status = 500;
        $error = [
          'message' => $e->getMessage(),
        ];

        if ($e instanceof AuthenticationException) {
            $status = 403;
        }

        if ($e instanceof AuthorizationException) {
            $status = 403;
        }

        if ($e instanceof ModelNotFoundException) {
            $status = 404;
        }

        if ($e instanceof NotFoundHttpException) {
            $status = 404;
            $error['message'] = 'Route not found';
        }

        if ($e instanceof ValidationException) {
            $status = 422;
            $error['data'] = $e->errors();
        }

        return response()->withError(
            $error,
            $status
        );

        // return parent::render($request, $e);
    }
}
