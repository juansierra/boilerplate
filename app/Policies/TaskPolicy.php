<?php

namespace App\Policies;

use App\User;
use App\Task;
use Illuminate\Auth\Access\HandlesAuthorization;

class TaskPolicy
{
    use HandlesAuthorization;

    /**
     * return True if the given user has a God role
     * @param  User    $user [description]
     * @return boolean       [description]
     */
    private function isUserAGod( User $user) {
      if ($user->hasRole(\App\Role::GOD)) {
          return true;
      }
    }

    /**
     * return True if the given task is owned by the given User
     * @param  Task    $task [description]
     * @param  User    $user [description]
     * @return boolean       [description]
     */
    private function isTaskOwnedByUser(Task $task, User $user)
    {
      return intval($user->id) === intval($task->user_id);
    }

    /**
     * Check before actual policy check
     * @param  User   $user
     * @param  string $ability
     * @return boolean
     */
    public function before(User $user, string $ability)
    {
        return $this->isUserAGod($user);
    }

    /**
     * Determine whether the user can view the task.
     *
     * @param  \App\User  $user
     * @param  \App\Task  $task
     * @return mixed
     */
    public function view(User $user, Task $task)
    {
        return $this->isTaskOwnedByUser($task, $user);
    }

    /**
     * Determine whether the user can create tasks.
     *
     * @param  \App\User  $user
     * @return mixed
     */
    public function create(User $user)
    {
        return true;
    }

    /**
     * Determine whether the user can update the task.
     *
     * @param  \App\User  $user
     * @param  \App\Task  $task
     * @return mixed
     */
    public function update(User $user, Task $task)
    {
        return $this->isTaskOwnedByUser($task, $user);
    }

    /**
     * Determine whether the user can delete the task.
     *
     * @param  \App\User  $user
     * @param  \App\Task  $task
     * @return mixed
     */
    public function delete(User $user, Task $task)
    {
        return intval($user->id) === intval($task->user_id);
    }

    /**
     * Determine whether the user can restore the task.
     *
     * @param  \App\User  $user
     * @param  \App\Task  $task
     * @return mixed
     */
    public function restore(User $user, Task $task)
    {
        return $this->isTaskOwnedByUser($task, $user);
    }

    /**
     * Determine whether the user can permanently delete the task.
     *
     * @param  \App\User  $user
     * @param  \App\Task  $task
     * @return mixed
     */
    public function forceDelete(User $user, Task $task)
    {
        return $this->isTaskOwnedByUser($task, $user);
    }
}
