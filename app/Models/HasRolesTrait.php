<?php
namespace App\Models;

trait HasRolesTrait
{
    /**
     * Get the roles for this user.
     *
     * @return \Illuminate\Database\Eloquent\Relations\belongsToMany
     */
    public function roles()
    {
        return $this->belongsToMany('App\Role');
    }

    /**
     * Get list of roles
     *
     * @return array
     */
    public function getRoleListAttribute($value)
    {
        return $this->roles->pluck('name')->implode(',');
    }

    /**
    * Check multiple roles
    *
    * @param  array   $roles
    * @return boolean
    */
    public function hasAnyRole(array $roles)
    {
      return null !== $this->roles()->whereIn('name', $roles)->first();
    }

    /**
    * Check one role
    *
    * @param  string  $role
    * @return boolean
    */
    public function hasRole(string $role)
    {
      return null !== $this->roles()->where('name', $role)->first();
    }
}
