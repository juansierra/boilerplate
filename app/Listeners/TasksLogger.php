<?php

namespace App\Listeners;

use App\Events\taskCreated;
use App\Events\TaskCompleted;

class TasksLogger
{
    /**
     * onTaskCompleted
     * @param  TaskCompleted $event
     * @return void
     */
    public function onTaskCompleted(TaskCompleted $event) {
        $task = $event->task;
        $user = $task->user;
        \Log::info("Task {$task->id} completed by user {$user->id}");
    }

    /**
     * onTaskCreated
     * @param  TaskCreated $event
     * @return void
     */
    public function onTaskCreated(TaskCreated $event) {
        $task = $event->task;
        $user = $task->user;
        \Log::info("Task {$task->id} created by user {$user->id}");
    }

    /**
     * Register the listeners for the subscriber.
     *
     * @param  \Illuminate\Events\Dispatcher  $events
     */
    public function subscribe($events)
    {
      $eventHandlers = [
          'App\Events\TaskCreated'    => 'App\Listeners\TasksLogger@onTaskCreated',
          'App\Events\TaskCompleted'  => 'App\Listeners\TasksLogger@onTaskCompleted',
      ];

      foreach ($eventHandlers as $event=>$handler) {
          $events->listen($event, $handler);
      }
    }
}
