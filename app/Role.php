<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Role extends Model
{
    public const GOD = 'god';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
    ];

    /**
     * Get the users with this role.
     *
     * @return \Illuminate\Database\Eloquent\Relations\belongsToMany
     */
    public function users()
    {
        return $this->belongsToMany('App\User');
    }
}
