<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\Hash;
use App\User;

class UserCreate extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'user:create {name?} {email?} {password?}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Creates a user';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $name = $this->argument('name');
        if (is_null($name)) {
            $name = $this->ask('What is your name?');
        }

        $email = $this->argument('email');
        if (is_null($email)) {
            $email = $this->ask('What is your email?');
        }

        $password = $this->argument('password');
        if (is_null($password)) {
            $password = $this->secret('What is your password?');
        }

        $user = User::create([
            'name' => $name,
            'email' => $email,
            'password' => Hash::make($password),
        ]);

        if (is_null($user)) {
            $this->error('Cannot create user.');
        } else {
            $this->info('User created.');
        }
    }
}
