<?php

namespace Tests\Feature\V1\Auth\Login;

use Tests\Feature\V1\TestBase;
use Illuminate\Support\Facades\Hash;
use App\User;

class PostTest extends TestBase
{
    protected $email = 'boilerplate@test.com';
    protected $password = 'secret';

    /**
     * Setup the test environment.
     *
     * @return void
     */
    protected function setUp() : void {
        parent::setUp();
        $this->artisan('passport:install');
    }

    /**
     * Can login with the right credentials
     *
     * @return void
     */
    public function testLogin()
    {
        $user = User::create([
            'name' => 'User',
            'email' => $this->email,
            'password' => Hash::make($this->password),
        ]);

        $this
            ->post(self::AUTH_LOGIN_URL, [
                'email' => $this->email,
                'password' => $this->password,
            ])
            ->assertStatus(200)
            ->assertJsonStructure([
                'status',
                'data' => [
                  'token',
                ]
            ]);
    }

    /**
     * Can't login with the wrong request params
     *
     * @return void
     */
    public function testLoginWithWrongParams()
    {
        $this
            ->post(self::AUTH_LOGIN_URL, [
                'email2' => $this->email,
                'password' => $this->password,
            ])
            ->assertStatus(422)
            ->assertJsonStructure([
                'status',
                'error' => [
                  'message',
                  'data' => [
                    'email',
                  ]
                ]
            ]);
    }

    /**
     * Can't login with missing request params
     *
     * @return void
     */
    public function testLoginWithMissingParams()
    {
        $this
            ->post(self::AUTH_LOGIN_URL, [
                'email' => $this->email,
            ])
            ->assertStatus(422)
            ->assertJsonStructure([
                'status',
                'error' => [
                  'message',
                  'data' => [
                    'password',
                  ]
                ]
            ]);
    }

}
