<?php

namespace Tests\Feature\V1\Auth\Me;

use Tests\Feature\V1\TestBase;
use App\User;

class GetTest extends TestBase
{
    /**
     * Can get /auth/me when authed
     *
     * @return void
     */
    public function testMeAuthed()
    {
        $user = $this->anyUser();

        $this
            ->actingAs($user, 'api')
            ->get(self::AUTH_ME_URL)
            ->assertStatus(200)
            ->assertJsonStructure([
              'status',
              'data' => [
                'email',
              ]
            ]);
    }

    /**
     * Can't get /auth/me when not authed
     *
     * @return void
     */
    public function testMeUnauthed()
    {
        $this
            ->get(self::AUTH_ME_URL)
            ->assertStatus(403)
            ->assertJsonStructure([
              'status',
              'error',
            ]);
    }
}
