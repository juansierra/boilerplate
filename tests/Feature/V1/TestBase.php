<?php

namespace Tests\Feature\V1;

use Tests\TestCase;
use Illuminate\Foundation\Testing\DatabaseTransactions;
// use Illuminate\Foundation\Testing\RefreshDatabase;
use App\User;

abstract class TestBase extends TestCase
{
    use DatabaseTransactions;

    /**
     * URLs
     */

    public const TASKS_URL      = '/api/v1/tasks';
    public const STATUS_URL     = '/api/v1/status';
    public const AUTH_LOGIN_URL = '/api/v1/auth/login';
    public const AUTH_ME_URL    = '/api/v1/auth/me';

    /**
     * Helper functions used by tests
     */


    /**
     * Return any existing user
     *
     * @return User
     */
    protected function anyUser() {
      return factory(User::class)->create();
    }

}
