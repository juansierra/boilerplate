<?php

namespace Tests\Feature\V1\Tasks;

use Tests\Feature\V1\TestBase;

class PostTest extends TestBase
{
    /**
     * Can post /tasks when authed
     *
     * @return void
     */
    public function testPostAuthed()
    {
        $user = $this->anyUser();

        $this
            ->actingAs($user, 'api')
            ->post(self::TASKS_URL, [
              'name' => 'My Task',
            ])
            ->assertStatus(201)
            ->assertJsonStructure([
              'status',
              'data' => [
                'name',
                'createdAt',
                'completedAt',
                'isCompleted',
              ],
            ]);
    }

    /**
     * Can't post /tasks when with missing params
     *
     * @return void
     */
    public function testPostWithMissingParams()
    {
      $user = $this->anyUser();

      $this
          ->actingAs($user, 'api')
          ->post(self::TASKS_URL, [
          ])
          ->assertStatus(422)
          ->assertJsonStructure([
            'status',
            'error',
          ]);
    }

    /**
     * Can't post /tasks when with missing params
     *
     * @return void
     */
    public function testPostWithWrongParams()
    {
      $user = $this->anyUser();

      $this
          ->actingAs($user, 'api')
          ->post(self::TASKS_URL, [
            'name' => '',
          ])
          ->assertStatus(422)
          ->assertJsonStructure([
            'status',
            'error',
          ]);
    }

    /**
     * Can't post /tasks when not authed
     *
     * @return void
     */
    public function testPostUnauthed()
    {
        $this
            ->post(self::TASKS_URL, [
              'name' => 'My Task',
            ])
            ->assertStatus(403)
            ->assertJsonStructure([
              'status',
              'error',
            ]);

    }
}
