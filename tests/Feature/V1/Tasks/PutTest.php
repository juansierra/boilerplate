<?php

namespace Tests\Feature\V1\Tasks;

use Tests\Feature\V1\TestBase;

class PutTest extends TestBase
{
    /**
     * Can put /tasks/{id} when authed
     *
     * @return void
     */
    public function testPutAuthed()
    {
        $user = $this->anyUser();

        $this
            ->actingAs($user, 'api')
            ->post(self::TASKS_URL, [
              'name' => 'My Task',
            ]);

        $this
            ->actingAs($user, 'api')
            ->put(self::TASKS_URL.'/1', [
              'name' => 'My Task Renamed',
            ])
            ->assertStatus(200)
            ->assertJsonStructure([
              'status',
              'data' => [
                'name',
                'createdAt',
                'completedAt',
                'isCompleted',
              ],
            ]);
    }

    /**
     * Can put /tasks/{id} when does not exist
     *
     * @return void
     */
    public function testPutNotFound()
    {
        $user = $this->anyUser();

        $this
            ->actingAs($user, 'api')
            ->delete(self::TASKS_URL.'/100')
            ->assertStatus(404)
            ->assertJsonStructure([
              'status',
              'error'
            ]);
    }

    /**
     * Can't put /tasks/{id} when not authed
     *
     * @return void
     */
    public function testPutUnAuthed()
    {
        $user = $this->anyUser();

        $this
            ->put(self::TASKS_URL.'/1', [
              'name' => 'My Task Renamed',
            ])
            ->assertStatus(403)
            ->assertJsonStructure([
              'status',
              'error',
            ]);
    }

}
