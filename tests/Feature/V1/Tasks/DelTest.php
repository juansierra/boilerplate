<?php

namespace Tests\Feature\V1\Tasks;

use Tests\Feature\V1\TestBase;

class DelTest extends TestBase
{
    /**
     * Can delete /tasks/{id} when authed
     *
     * @return void
     */
    public function testDelAuthed()
    {
        $user = $this->anyUser();

        $response = $this
            ->actingAs($user, 'api')
            ->post(self::TASKS_URL, [
              'name' => 'My Task',
            ]);

        $task = json_decode($response->getContent());

        $this
            ->actingAs($user, 'api')
            ->delete(self::TASKS_URL.'/'.$task->data->id)
            ->assertStatus(200)
            ->assertJsonStructure([
              'status',
              'data' => [
                'name',
                'createdAt',
                'completedAt',
                'isCompleted',
              ],
            ]);
    }

    /**
     * Can delete /tasks/{id} when does not exist
     *
     * @return void
     */
    public function testDelNotFound()
    {
        $user = $this->anyUser();

        $this
            ->actingAs($user, 'api')
            ->delete(self::TASKS_URL.'/100')
            ->assertStatus(404)
            ->assertJsonStructure([
              'status',
              'error'
            ]);
    }

    /**
     * Can't delete /tasks/{id} when not authed
     *
     * @return void
     */
    public function testDelUnAuthed()
    {
        $user = $this->anyUser();

        $this
            ->delete(self::TASKS_URL.'/1')
            ->assertStatus(403)
            ->assertJsonStructure([
              'status',
              'error',
            ]);
    }

}
