<?php

namespace Tests\Feature\V1\Tasks;

use Tests\Feature\V1\TestBase;

class GetTest extends TestBase
{
    /**
     * Can get /tasks when authed
     *
     * @return void
     */
    public function testIndexAuthed()
    {
        $user = $this->anyUser();

        $this
            ->actingAs($user, 'api')
            ->get(self::TASKS_URL)
            ->assertStatus(200)
            ->assertJsonStructure([
              'status',
              'data',
            ]);
    }

    /**
     * Can't get /tasks when not authed
     *
     * @return void
     */
    public function testIndexUnauthed()
    {
        $this
            ->get(self::TASKS_URL)
            ->assertStatus(403)
            ->assertJsonStructure([
              'status',
              'error',
            ]);

    }
}
