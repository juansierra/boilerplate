<?php

namespace Tests\Feature\V1\Status;

use Tests\Feature\V1\TestBase;

class GetTest extends TestBase
{
    /**
     * Can get /status when authed
     *
     * @return void
     */
    // public function testStatusAuthed()
    // {
    //     $user = factory(User::class)->create();
    //
    //     $this
    //         ->actingAs($user, 'api')
    //         ->get(self::STATUS_URL)
    //         ->assertStatus(200)
    //         ->assertJsonStructure([
    //           'status',
    //           'data' => [
    //             'env',
    //           ]
    //         ]);
    // }

    /**
     * Can't get /status when not authed
     *
     * @return void
     */
    public function testStatusUnauthed()
    {
        $this
            ->get(self::STATUS_URL)
            ->assertStatus(200)
            ->assertJsonStructure([
              'status',
              'data' => [
                'env',
              ]
            ]);

    }
}
